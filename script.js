
//запрос AJAX
 let httpRequest = new XMLHttpRequest();
 httpRequest.open("GET", "https://ajax.test-danit.com/api/swapi/films", true);
 httpRequest.onreadystatechange = function () {
   if (this.readyState === 4 && this.status == 200) {
       data = this.responseText
       showFilmList(JSON.parse(data))
    }
 };
httpRequest.send();

//запрос fetch
//function fetchFilms() {
//   fetch("https://ajax.test-danit.com/api/swapi/films")
//     .then(response => response.json())
//     .then(data => showFilmList(data))
//    .catch(error => console.error(error));
//};
//fetchFilms()

//Вставка строки фильмов на строку
function showFilmList(filmsArr) {
   const propsFilmForShow = ['episodeId', 'name', 'openingCrawl'];
   const rootDiv = document.querySelector('#root');
   const loaderString = `
   <div class="box"><div class="container">
      <span class="circle"></span><span class="circle"></span>
      <span class="circle"></span><span class="circle"></span>
   </div></div>`;

   filmsArr.sort((x, y) => x.episodeId - y.episodeId);
   filmsArr.forEach((obj, i) => {
      try {
         let filmString = crateLiString(obj, propsFilmForShow);
         rootDiv.insertAdjacentHTML('beforeend', `<ul style='text-align: left; padding: 10px;'>${filmString}
         <li><strong>Characters</strong> - ${loaderString}</li></ul>`);

         //запрос персонажей с .then від Promise.all
         fetchAllCharacters(obj.characters)
            .then((data) => {
               //убрать лоадер после загрузки
               const loader = rootDiv.children[i].querySelector(".box");
               loader.style.display = "none";

               //для каждого obj вставить его .name
               data.forEach(char => {
                  rootDiv.children[i].lastChild.insertAdjacentHTML('beforeend', `${char.name}`);
               })
            }).catch((err) => {
               console.error('Promise.all rejection', err);
            })
      } catch (error) {
         console.log(error);
      }
   })
};

//создание строки
function crateLiString(obj, showProp) {
   let listString = '';
   for (const [key, value] of Object.entries(obj)) {
      if (showProp.includes(key)) {
         listString += `<li><strong>${key}</strong> - ${value},</li>`;
      }
   }
   return listString;
}

//Запрос персонажей
function fetchAllCharacters(charArr) {
   const fetchArr = charArr.map((char) =>
      fetch(char)
         .then((response) => response.json())
         .catch((error) => console.error(error))
   );
   return Promise.all(fetchArr);
};
